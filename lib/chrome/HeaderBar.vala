namespace Abstract.Chrome {
    public class HeaderBar {
        bool hasTabs = false;
        bool hasHistoryBtn = false;
        bool hasSearchBar = false;
        
        public Gtk.HeaderBar headerBar;
        public Gtk.SearchEntry searchBar;
        Abstract.UI.Button back;
        Abstract.UI.Button forward;
        
        public HeaderBar(bool _hasTabs, bool _hasHistoryBtn, bool _hasSearchBar){
            this.hasTabs = _hasTabs;
            this.hasHistoryBtn = _hasHistoryBtn;
            this.hasSearchBar = _hasSearchBar;
            
            this.headerBar = new Gtk.HeaderBar();
            this.searchBar = new Gtk.SearchEntry();
            
            this.display();
        }
        
        public void display(){
            
            if(hasHistoryBtn == true){
                this.back = new Abstract.UI.Button.from_icon_name("go-previous");
                this.forward = new Abstract.UI.Button.from_icon_name("go-next");
                
                this.headerBar.pack_start(this.back);
                this.headerBar.pack_start(this.forward);
            }
            
            if(hasSearchBar == true){
                this.headerBar.pack_start(this.searchBar);
            }
            
            this.headerBar.pack_start(new Gtk.Separator(Gtk.Orientation.VERTICAL));
            
            Gtk.Box _box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
            _box.append(new Gtk.Button.with_label("TAB 1"));
            _box.append(new Gtk.Button.with_label("TAB 2"));
            _box.append(new Gtk.Button.with_label("TAB 3"));
            
            this.headerBar.set_title_widget(_box);
            
            this.headerBar.pack_end(new Gtk.Separator(Gtk.Orientation.VERTICAL));
            
            
            Abstract.UI.Button newTab_btn = new Abstract.UI.Button.from_icon_name("tab-new");
            Abstract.UI.Button menu_btn = new Abstract.UI.Button.from_icon_name("open-menu");
            
            this.headerBar.pack_end(newTab_btn);
            this.headerBar.pack_end(menu_btn);
            
            this.headerBar.pack_end(new Gtk.Separator(Gtk.Orientation.VERTICAL));
            
            this.headerBar.show();
            
        }
        
        public Gtk.HeaderBar bar(){
            return this.headerBar;
        }
    }
}
