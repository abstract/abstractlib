namespace Abstract.Chrome {
    public class TabWindow : Gtk.ApplicationWindow {
        bool hasTabs = false;
        bool hasHistoryBtn = false;
        bool hasSearchBar = false;
        
        Gtk.Box contentBox;
        
        public TabWindow(Gtk.Application _application, bool _hasTabs, bool _hasHistoryBtn, bool _hasSearchBar){
            this.hasTabs = _hasTabs;
            this.hasHistoryBtn = _hasHistoryBtn;
            this.hasSearchBar = _hasSearchBar;
            this.application = _application;
            
            this.display();
        }
        
        public void display(){
            this.contentBox = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
            
            Abstract.Chrome.HeaderBar hb = new Abstract.Chrome.HeaderBar(this.hasTabs, this.hasHistoryBtn, this.hasSearchBar);
            
            this.set_titlebar(hb.headerBar);
            
            this.child = contentBox;
        }
    }
}
