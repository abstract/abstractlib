namespace Abstract.UI {
    public class Button : Gtk.Button {
        public Button.from_icon_name(string name){
            this.set_icon_name(name);
        }
        
        public Button.with_label(string label){
            this.set_label(label);
        }
    }
}
